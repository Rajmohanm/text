package DailyAssignments;

import java.util.Scanner;

public class FindSumOfAllMultiples {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum=0;
		Scanner num = new Scanner(System.in);
		System.out.println("Enter the number :");
		int inp= num.nextInt();
		for(int i=1;i<inp;i++)
		{	
			if(i%3==0 || i%5==0)
			{
				sum += i;
			    
			}
		     
		}
		System.out.println("Sum of Multiples of 3 or 5 less than 100 is " + sum);
		

	}

}
