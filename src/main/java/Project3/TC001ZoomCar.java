package Project3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Project3.ProjectMethods;

public class TC001ZoomCar extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "Zoom Car";
		testDesc = "Book car";
		author = "Sathish";
		category = "Project Day";
	}

	@Test()
	public void findCar() {

		List<WebElement> prices = driver.findElementsByXPath("//div[@class='price']");
		List<WebElement> carNames = driver.findElementsByXPath("//div[@class='details']/h3");

		Map<Integer, String> map = new TreeMap<>();
		for (int i = 0; i < prices.size(); i++) {
			map.put(Integer.parseInt(prices.get(i).getText().replaceAll("\\D", "")), carNames.get(i).getText());
			// System.out.println("Car names" +map);
		}
	}
}
