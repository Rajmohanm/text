package week4.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IphoneXprice {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com/");
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
		driver.findElementByXPath("//input[@class='LM6RPg']").sendKeys("iphone x",Keys.ENTER);
		List<WebElement> iPhoneList = driver.findElementsByXPath("//div[@class='_1vC4OE']");
		List<Integer> priceList=new ArrayList<>();
		for (int i = 0; i <= iPhoneList.size()-1; i++) {
			
			
					String string = iPhoneList.get(i).getText();
							String str = string.replaceAll("\\D", "");
					int replaceAll=Integer.parseInt(str);
			priceList.add(replaceAll);
		}
		
		Set<Integer> set=new HashSet<>();
		set.addAll(priceList);
		int max=0;
		for(int a:set) {
			if(a>max)
				max=a;}
		System.out.println("First maximum price "+max);
		int max2=0;
		for (Integer b : set) {
			
			if(b>max2&&b!=max)
				max2=b;}
		System.out.println("Second maximum price "+max2);
		//String price = driver.findElementByXPath("(//div[@class='_1vC4OE'])[2]").getText();
				//System.out.println(price);

		
	}

}
