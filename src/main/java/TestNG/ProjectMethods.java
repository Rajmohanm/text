package TestNG;

import org.testng.annotations.AfterClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import week4.day2.SeMethods;

public class ProjectMethods extends SeMethods {

	@BeforeSuite
	public void beforesuite() {
		System.out.println("Before suite is called");
	}

	@BeforeTest
	public void beforetest() {
		System.out.println("Before test is called");
	}

	@BeforeClass

	public void beforeclass() {
		System.out.println("Before class is called");
	}

	@BeforeMethod(groups = { "any" })
	public void Login() {

		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);

		click(locateElement("linkText", "CRM/SFA"));

	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("After Method is called");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("After class  is called");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("After Test is called");
	}

}
