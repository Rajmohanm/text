package week6Day1Attributes;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import TestNG.ProjectMethods;

public class TC002EditLead extends ProjectMethods {

	//@Test(groups = { "sanity" }, dependsOnGroups= {"smoke"})
	//@Test(invocationCount=1, timeOut=15000)
	@Test
	public void Editmethod() {
		click(locateElement("linkText", "Create Lead"));
		WebElement cpyname = locateElement("id", "createLeadForm_companyName");
		type(cpyname, "HCL");

		WebElement fname = locateElement("id", "createLeadForm_firstName");
		type(fname, "sathish");

		WebElement lname = locateElement("id", "createLeadForm_lastName");
		type(lname, "mani");
		WebElement CreateLead = locateElement("class", "smallSubmit");
		click(CreateLead);
		WebElement editLead = locateElement("xpath", "(//a[@class='subMenuButton'])[3]");
		click(editLead);
		
		

	}

}
