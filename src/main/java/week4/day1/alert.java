package week4.day1;

import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class alert {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		/*
		 * WebElement iframe = driver.findElementByName("iframexpath"); ChromeDriver
		 * Webdriverwait = Webdriverwait("");
		 */

		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		WebElement frame = driver.findElementById("iframeResult");
		driver.switchTo().frame(frame);
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alert = driver.switchTo().alert();
		
		File scrFile= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./snap/img1.jpeg"));
		String alertMsg = alert.getText();
		System.out.println(alertMsg);
		alert.sendKeys("Sathish");
		alert.accept();
		String text = driver.findElementByXPath("//p[@id='demo']").getText();

		if (text.contains("Sathish")) {
			System.out.println("Insert the name is correct");
		} else {
			System.out.println("Insert the name is not matched");
			System.out.println(text);
		}

	}

}
