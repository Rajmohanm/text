package Week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestCaseDataProvider extends ProjectMethods {

	@Test(dataProvider = "qa")
	public void createLead(String cpyname1, String fname1, String lname1, String mail, String number) {


		click(locateElement("linkText", "Create Lead"));
		WebElement cpyname = locateElement("id", "createLeadForm_companyName");
		type(cpyname, cpyname1);

		WebElement fname = locateElement("id", "createLeadForm_firstName");
		type(fname, fname1);

		WebElement lname = locateElement("id", "createLeadForm_lastName");
		type(lname, lname1);

		WebElement CreateLead = locateElement("class", "smallSubmit");
		click(CreateLead);

	}

	@DataProvider(name = "qa")
	public Object[][] fetchData() {
		Object[][] data = new Object[2][5];
		data[0][0] = "Testleaf";
		data[0][1] = "sarath";
		data[0][2] = "M";
		data[0][3] = "sarath@TestLeaf.com";
		data[0][4] = "1234567";

		data[1][0] = "HCl";
		data[1][1] = "sathish";
		data[1][2] = "M";
		data[1][3] = "sathish@hcl.com";
		data[1][4] = "1232423";
		return data;
	}

}
