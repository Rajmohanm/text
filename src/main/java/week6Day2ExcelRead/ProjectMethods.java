package week6Day2ExcelRead;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import week4.day2.SeMethods;


	
	public class ProjectMethods extends SeMethods {
		
		@BeforeMethod
		public void Login() {
			startApp("chrome", "http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("password");
			type(elePassword, "crmsfa");
			WebElement eleLogin = locateElement("class","decorativeSubmit");
			click(eleLogin);
			click(locateElement("linkText","CRM/SFA"));
		}
		@AfterMethod
		public void closeApp() {
			closeBrowser();
		}


}
