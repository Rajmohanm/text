package DailyAssignments;

import java.util.Scanner;

public class StringPalindromeornot {

	public static void main(String[] args) {
		//String is Palindrome or Not
	Scanner getinput = new Scanner(System.in);
	System.out.println("Enter the string :");
	
	String orginal, reverse="";
	orginal = getinput.next();
	System.out.println(orginal);
	int  n = orginal.length();
	
	for(int i=n-1;i>=0;i--)
	{
		reverse = reverse + orginal.charAt(i);
	}
	if(orginal.equalsIgnoreCase(reverse))
	{
		System.out.println("Given string in palindrome");
	}
	else
	{
		System.out.println("Given string is not palindrome");
	}
	
	}
}
