package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectDropdownList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int i;
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//Load the URL
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		//maximize the window
		driver.manage().window().maximize();
		driver.findElementByLinkText("Sign up").click();
		WebElement country =driver.findElementById("userRegistrationForm:countries");
		Select dropdown=new Select(country);
		List<WebElement> allob=dropdown.getOptions();
		for (WebElement eachop : allob) {			
		System.out.println(eachop.getText());
		}

		}

}
