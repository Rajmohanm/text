package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class Leaftaps {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get("http://leaftaps.com/opentaps");
		Thread.sleep(2000);
		driver.findElementByXPath("//input[@type='text']").sendKeys("DemoSalesManager");

		driver.findElementByXPath("//input[@type='password']").sendKeys("crmsfa");

		driver.findElementByXPath("//input[@value='Login']").click();
		driver.findElementByXPath("//div[@id='button']//following::a[1]").click();
		driver.findElementByXPath("//div[@class='frameSectionBody']//following::li[1]").click();

		driver.findElementByXPath("//input[@name='companyName' and @id ='createLeadForm_companyName']").sendKeys("Hcl");
		driver.findElementByXPath("//img[@src='/images/fieldlookup.gif']");
		// driver.findElementByXPath("//input[@name='parentPartyId']").sendKeys("1234");

		driver.findElementByXPath("//input[@id='createLeadForm_firstName']").sendKeys("Sathish");

		driver.findElementByXPath("//input[@id='createLeadForm_lastName']").sendKeys("Kumar");
		driver.findElementByXPath("//select[@class='inputBox']/option[text()='Conference']").click();
		driver.findElementByXPath("//select[@id='createLeadForm_marketingCampaignId']/option[text()='Automobile']")
				.click();
		Thread.sleep(5000);

		driver.findElementByXPath("//select[@class='inputBox']/option[text()='Employee']").click();
		driver.findElementByXPath("//input[@class='smallSubmit']").click();
	}

}
