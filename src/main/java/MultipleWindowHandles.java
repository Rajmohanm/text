import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class MultipleWindowHandles {

	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/popup.php");
		driver.findElementByLinkText("Click Here").click();
		Set<String> windowHandles = driver.getWindowHandles();
		//System.out.println(windowHandles);
		List<String> list1  = new ArrayList<>();
		list1.addAll(windowHandles);
		driver.switchTo().window(list1.get(1));
		driver.findElementByName("emailid").sendKeys("sathish16aug@gmail.com");
		driver.findElementByName("btnLogin").click();
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snap/img.jpeg");
		FileUtils.copyFile(src, des);
		Thread.sleep(2000);
		driver.switchTo().window(list1.get(1)).close();

	}

}
