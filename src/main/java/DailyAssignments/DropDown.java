package DailyAssignments;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown{

public static void main(String[] args) {
	
	int i=0;
	
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();

		//Load the URL
		driver.get("http://testleaf.herokuapp.com/pages/Dropdown.html");

		//maximize the window
		driver.manage().window().maximize();

		WebElement  sample=driver.findElementById("dropdown1");

		Select drop=new Select(sample);

		List<WebElement> alloption = drop.getOptions();

		int lastoption = alloption.size()-1;
		System.out.println(lastoption);

		for (WebElement eachop : alloption) {
			
			if (i==lastoption) {
				eachop.click();
			
			}
			i++;
		}		
	}
}
