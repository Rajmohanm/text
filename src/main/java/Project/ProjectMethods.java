package Project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ProjectMethods extends SeMethods {

	@BeforeTest
	public void beforeTest() {
		System.out.println("@BeforeTest");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}

	@BeforeMethod
	public void login() {
		beforeMethod();

		startApp("chrome", "https://www.zoomcar.com/chennai/");

		WebElement search = locateElement("xpath", "//a[@class='search']");
		click(search);
		WebElement select = locateElement("xpath", "(//div[@class='items'])[1]");
		click(select);

		WebElement login = locateElement("xpath", "//button[text()='Next']");
		click(login);

		// Get the current date
		Date date = new Date();
// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd");
// Get today's date
		String today = sdf.format(date);
// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today) + 1;
// Print tomorrow's date
		System.out.println(tomorrow);
		WebElement pick = locateElement("xpath", "//div[contains(text()," + tomorrow + ")]");
		click(pick);

		WebElement login1 = locateElement("xpath", "//button[text()='Next']");
		click(login1);

		WebElement done = locateElement("xpath", "//button[text()='Done']");
		click(done);

	}

	@AfterMethod
	public void closeApp() {
		//closeBrowser();
	}

	@AfterClass
	public void afterClass() {
		System.out.println("@AfterClass");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("@AfterTest");
	}

	@AfterSuite
	public void afterSuite() {
		endResult();
	}

	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}

}
