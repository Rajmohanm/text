package SelfPractice;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Flipkart {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.google.com/");
		driver.findElementById("lst-ib").sendKeys("flipkart");
		driver.findElementByXPath("//input[@value='Google Search']").sendKeys(Keys.ENTER);
		// driver.findElementByXPath("(//input[@class='gsfi'])[1]").click();
		// driver.close();
		Thread.sleep(2000);
		driver.findElementByXPath("//a[text()='Mobile Phones']").click();
		driver.findElementByXPath("//a[text()='Login & Signup']").click();
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		/*
		 * String tedriver.findElementByXPath("//div[@class='_2kSfQ4']").getText();
		 * System.out.println(text);
		 */
		List<WebElement> mPrice = driver.findElementsByClassName("_3o3r66");
		
		List<Integer> mobiles = new ArrayList<>();

		for (int i = 0; i <= mPrice.size() - 1; i++) {
			String text = mPrice.get(i).getText();
			String str = text.replaceAll("\\D", "");
			int replaceAll = Integer.parseInt(str);
			mobiles.add(replaceAll);
		}
		System.out.println(mobiles);
		
	}

}
