package Project2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Project2.SeMethods;

public class ProjectMethods extends SeMethods {

	@Test
	public void login() throws InterruptedException {

		startApp("chrome", "https://www.facebook.com/#");

		WebElement eleUserName = locateElement("xpath", "(//input[@class='inputtext'])[1]");
		type(eleUserName, "sathish16aug@gmail.com");

		WebElement elePassword = locateElement("xpath", "(//input[@type='password'])[1]");
		type(elePassword, "");

		WebElement login = locateElement("xpath", "//input[@value='Log In']");
		click(login);

		driver.findElementByXPath("//input[@class='_1frb']").sendKeys("Testleaf Chennai", Keys.ENTER);

		WebElement Testleaf = locateElement("xpath", "(//button[@value='1'])[2]");
		click(Testleaf);

		String Test = locateElement("xpath", "//i[@class='_3-8_ img sp_dKN9RhpHtm1 sx_1e0773']").getText();
		if (Test.equalsIgnoreCase("Like")) {
			click("Like");

		} else {
			System.out.println("Already liked");
		}

		WebElement Review = locateElement("xpath", "(//span[@class='_2yav'])[2]");
		click(Review);
		String text = locateElement("xpath", "(//div[@class='mvm uiP fsm'])[2]").getText();
		System.out.println(text);
	}

}
