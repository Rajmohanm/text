package week3.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Irctcallrows {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU", Keys.TAB);
		WebElement ele = driver.findElementById("chkFirstLast");
		if (ele.isSelected()) {
			ele.click();
		}
		Thread.sleep(3000);

		WebElement table = driver.findElementByXPath("//table[@class = 'DataTable TrainList']");
		List<WebElement> allrows = table.findElements(By.tagName("td"));
		System.out.println(allrows.size());

		 WebElement secrow = allrows.get(1);

		
		  List<WebElement> allcol = secrow.findElements(By.tagName("td"));
		  
		  System.out.println(allcol.size());
		 

		
		  for (WebElement eachrow : allrows) {
		  
		  List<WebElement> cols = eachrow.findElements(By.tagName("td")); 
		  String text = cols.get(1).getText();
		  
		  System.out.println(text); }
		 
		/*for (int i = 0; i < allrows.size(); i++) {
			List<WebElement> allcol = allrows.get(i).findElements(By.tagName("td"));

			String text = allcol.get(i).getText();

			System.out.println(text);

		}

*/	}

}
