import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class Leaftaps {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();

		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@type='text']").sendKeys("DemoSalesManager");

		driver.findElementByXPath("//input[@type='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@value='Login']").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> list1=new ArrayList<String>();
	 list1.addAll(windowHandles);
	 driver.switchTo().window(list1.get(1));
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Sathish");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("(//a[text()='Sathish'])[1]").click();
		 driver.switchTo().window(list1.get(0));
			driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Set<String> windowHandles1 = driver.getWindowHandles();
		List<String> list2=new ArrayList<String>();
		 list2.addAll(windowHandles1);
		 
		driver.switchTo().window(list2.get(1));
	
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Ganesh");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("(//a[text()='Ganesh'])[1]").click();
		Thread.sleep(3000);
		driver.switchTo().window(list2.get(0));
		
		driver.findElementByClassName("buttonDangerous").click();
		String text = driver.switchTo().alert().getText();
		if(text.contains("Are you sure?"))
		{
		   driver.switchTo().alert().accept();
		}
		   driver.findElementByXPath("//a[text()='Find Leads']").click();

	}

}
