package week4.day2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IphoneX {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://www.flipkart.com/");
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		Thread.sleep(2000);
		driver.findElementByXPath("//input[@type='text']").sendKeys("Iphone X", Keys.ENTER);
		Thread.sleep(2000);
		List<WebElement> iPhoneList = driver.findElementsByClassName("_1vC4OE");
		List<Integer> priceList = new ArrayList<>();
		for (int i = 0; i <= iPhoneList.size() - 1; i++) {

			String string = iPhoneList.get(i).getText();
			String str = string.replaceAll("\\D", "");
			int replaceAll = Integer.parseInt(str);
			priceList.add(replaceAll);
		}

		Set<Integer> set = new HashSet<>();
		set.addAll(priceList);
		int max = 0;
		for (int a : set) {
			if (a > max)
				max = a;
		}
		System.out.println("First maximum price " + max);
		int max2 = 0;
		for (Integer b : set) {

			if (b > max2 && b != max)
				max2 = b;
		}
		System.out.println("Second maximum price " + max2);

	}

}
