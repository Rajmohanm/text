package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class LeafgroundDropdown {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("http://leafground.com/pages/Dropdown.html");
		Thread.sleep(5000);
		driver.findElementByXPath("//select[@id='dropdown1']/option[@value='4']").click();
		driver.findElementByXPath("//select[@name='dropdown2']/option[@value='4']").click();
		driver.findElementByXPath("//select[@id='dropdown3']/option[@value='4']").click();
		
		
		driver.findElementByXPath("//select[@class='dropdown']/option[@value='4']").click();
		driver.findElementByXPath("//select[@id='dropdown3']/option[@value='4']").click();
		Thread.sleep(5000);
		driver.findElementByXPath("(//div[@class='example']/following::option[@value='4'])[4]");
		
		driver.findElementByXPath("(//div[@class='example']/following::option[@value='4'])[5]");
		
		
	}

}
