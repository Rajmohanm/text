package TestNG;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import TestNG.ProjectMethods;

public class TC002EditLead  extends ProjectMethods {

	@Test
	public void Editmethod() {
		// TODO Auto-generated method stub
		
		
		click(locateElement("linkText","Create Lead"));
		WebElement cpyname =  locateElement("id","createLeadForm_companyName");
		type(cpyname, "HCL");

		WebElement fname =  locateElement("id","createLeadForm_firstName");
		type(fname, "sathish");

		WebElement lname =  locateElement("id","createLeadForm_lastName");
		type(lname, "mani");
		WebElement CreateLead = locateElement("class","smallSubmit");
		click(CreateLead);

	}

}
