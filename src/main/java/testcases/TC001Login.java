package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import week4.day2.SeMethods;

public class TC001Login extends SeMethods{

	/*@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
	}*/
	
	@Test
	public void login() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
	
		click(locateElement("linkText","CRM/SFA"));
		click(locateElement("linkText","Create Lead"));
		Thread.sleep(2000);
		
		/*click(locateElement("linkText","My Leads"));
		
	//	click(locateElement("xpath",");
		
		WebElement CreateLead1 = locateElement("xpath","(//a[text()='first04'])[1]");
		click(CreateLead1);
		
		WebElement CreateLead2 = locateElement("linkText","Edit");
		click(CreateLead2);
		
	//	click(locateElement("linkText","Edit"));
		Thread.sleep(2000);
		driver.findElementById("updateLeadForm_firstName").clear();
		Thread.sleep(2000);
		WebElement fname1 =  locateElement("id","updateLeadForm_firstName");
		type(fname1, "sathish");
		
		WebElement update = locateElement("class","smallSubmit");
		click(update);*/
		
		click(locateElement("linkText","Create Lead"));
		Thread.sleep(2000);
	
		WebElement cpyname =  locateElement("id","createLeadForm_companyName");
		type(cpyname, "HCL");

		WebElement fname =  locateElement("id","createLeadForm_firstName");
		type(fname, "sathish");

		WebElement lname =  locateElement("id","createLeadForm_lastName");
		type(lname, "mani");
		
		WebElement Source =  locateElement("id","createLeadForm_dataSourceId");
		type(Source, "Employee");
		
		WebElement drop =  locateElement("id","createLeadForm_marketingCampaignId");
		type(drop, "Automobile");

		WebElement Currency =  locateElement("id","createLeadForm_currencyUomId");
		type(Currency, "INR - Indian Rupee");
		
		WebElement Industry =  locateElement("id","createLeadForm_industryEnumId");
		type(Industry, "Manufacturing");
		
		WebElement Ownership =  locateElement("id","createLeadForm_ownershipEnumId");
		type(Ownership, "LLC/LLP");
		
		WebElement CreateLead = locateElement("class","smallSubmit");
		click(CreateLead);
		
		WebElement Edit = locateElement("xpath","//a[text()='Edit']");
		click(Edit);
		driver.findElementById("updateLeadForm_firstName").clear();
		Thread.sleep(2000);
		
		WebElement name =  locateElement("id","updateLeadForm_firstName");
		type(name, "kumar");
		Thread.sleep(2000);
		WebElement update = locateElement("xpath","(//input[@name='submitButton'])[1]");
		click(update);
		
		WebElement Delete = locateElement("xpath","//a[text()='Delete']");
		click(Delete);
		
		WebElement Merge = locateElement("xpath","//a[text()='Merge Leads']");
		click(Merge);
		
		
		WebElement window1 = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[1]");
		click(window1);
		
		Set<String> firstwindow = driver.getWindowHandles();
		List<String> list = new ArrayList<>();
		list.addAll(firstwindow);
		
		driver.switchTo().window(list.get(1));

		
		WebElement findleadlink = locateElement("xpath","//input[@name='firstName']");
		type(findleadlink, "sathish");
		
		
		WebElement findlead = locateElement("xpath","//button[text()='Find Leads']");
		click(findlead);
		driver.switchTo().window(list.get(0));
		
	
		
	}

}












