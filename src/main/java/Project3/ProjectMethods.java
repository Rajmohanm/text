package Project3;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ProjectMethods extends SeMethods {

	@BeforeTest
	public void beforeTest() {
		System.out.println("@BeforeTest");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}

	@BeforeMethod
	public void login() {
		beforeMethod();

		startApp("chrome", "https://www.zoomcar.com/chennai/");
		click(locateElement("link", "Start your wonderful journey"));
		click(locateElement("xpath", "//div[@class='heading']/following-sibling::div"));
		click(locateElement("class", "proceed"));
		// Date format code
		click(locateElement("xpath", "//div[contains(text(),'21')]"));
		click(locateElement("class", "proceed"));
		click(locateElement("class", "proceed"));

	}

	private List<WebElement> locateElements(String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}

	@AfterClass
	public void afterClass() {
		System.out.println("@AfterClass");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("@AfterTest");
	}

	@AfterSuite
	public void afterSuite() {
		endResult();
	}

	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}

}
