package week6Day2;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		XSSFWorkbook wb = new XSSFWorkbook("./Excel/Input.xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		
		System.out.println("Number of rows count:");

		int rowcount = sheet.getLastRowNum();
		System.out.println(rowcount);
		
		System.out.println("Number of col count:");
		
		short cellcount = sheet.getRow(0).getLastCellNum();
		System.out.println(cellcount +"\n"+"-------------------");
	

		for (int j = 1; j <= rowcount; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < cellcount; i++) {
				XSSFCell cell = row.getCell(i);
				/*try {
					String value = cell.getStringCellValue();
					System.out.println(value);
					
				} catch (NullPointerException e) {

					System.out.println(e);
				}*/
				
				String value="";
				CellType cellTypeEnum = cell.getCellTypeEnum();
				if(cellTypeEnum == CellType.STRING)
				{
					value = cell.getStringCellValue();
				}
				else if(cellTypeEnum == CellType.NUMERIC)
				{
					value = ""+(long)cell.getNumericCellValue();
				}
				
			} 
			System.out.println("*************************");
			
		}
		wb.close();

	}

}
