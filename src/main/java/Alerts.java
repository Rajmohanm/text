import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alerts {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
	
	
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
	Thread.sleep(2000);
	WebElement frame = driver.findElementById("iframeResult");
	driver.switchTo().frame(frame);
	driver.findElementByXPath("//button[text()='Try it']").click();
	
	
	Alert alert = driver.switchTo().alert();
	alert.sendKeys("Sathish");
	alert.accept();
	String text = driver.findElementByXPath("//p[@id='demo']").getText();
	
	if(text.contains("Sathish"))
	{
		System.out.println("Inserted name is successfully");
	}
	else
	{
		System.out.println("Not Inserted successfully");
	}
	}

}
