package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Selenium {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("http://leaftaps.com/opentaps");
		driver.findElementByName("USERNAME").sendKeys("DemoSalesManager");

		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();

		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByClassName("inputBox").sendKeys("HCL");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sathish");
		driver.findElementById("createLeadForm_lastName").sendKeys("Kumar");

		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source);
		dd.selectByVisibleText("Public Relations");

		WebElement source1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(source1);
		dd1.selectByValue("CATRQ_AUTOMOBILE");
		
		WebElement source2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2 = new Select(source2);
	//	dd2.getAllSelectedOptions();

		List<WebElement> alloptions = 	dd2.getOptions();
		
		for(int i=0; i<alloptions.size();i++)
		{
			//if(alloptions.get(i).getText().startsWith("M"))
			//{
				System.out.println(alloptions.get(i).getText());
			//}
		 }
				
	
		
		
		/*for(WebElement eachelement : alloptions) {
			
			if(eachelement.getText().startsWith("M"))
			{
				System.out.println(eachelement.getText());
			}
		

		}*/
		
	}

}
