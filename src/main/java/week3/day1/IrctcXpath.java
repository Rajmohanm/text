package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcXpath {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementByXPath("//input[@id='userRegistrationForm:userName']").sendKeys("Sathish00000000");
		driver.findElementByXPath("//a[text()='Check Availability']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//input[@name='userRegistrationForm:password']").sendKeys("kumar");
		driver.findElementByXPath("//input[@name='userRegistrationForm:confpasword']").sendKeys("kumar");
		driver.findElementByXPath(
				"//select[@name='userRegistrationForm:securityQ']/option[text()='Who was your Childhood hero?']")
				.click();
		driver.findElementByXPath("//input[@name='userRegistrationForm:securityAnswer']").sendKeys("vijay");
		driver.findElementByXPath("//select[@name='userRegistrationForm:prelan']/option[@value='hi']");

		driver.findElementByXPath("//input[@id='userRegistrationForm:firstName']").sendKeys("sathish");
		driver.findElementByXPath("//input[@id='userRegistrationForm:middleName']").sendKeys("kumar");
		driver.findElementByXPath("//input[@id='userRegistrationForm:lastName']").sendKeys("mani");
		driver.findElementByXPath("//input[@name='userRegistrationForm:gender' and @value='M']").click();
		driver.findElementByXPath("//label[text()=' Unmarried']").click();
		driver.findElementByXPath("//select[@name='userRegistrationForm:dobDay']/option[@value='27']").click();
		driver.findElementByXPath("//select[@name='userRegistrationForm:dobMonth']/option[@value='07']").click();
		driver.findElementByXPath("//select[@name='userRegistrationForm:dateOfBirth']/option[@value='1988']").click();
		driver.findElementByXPath("//select[@name='userRegistrationForm:occupation']/option[@value='3']").click();
		driver.findElementByXPath("//input[@name='userRegistrationForm:uidno']").sendKeys("123456");
		driver.findElementByXPath("//input[@name='userRegistrationForm:idno']").sendKeys("123456");
		driver.findElementByXPath("//select[@name='userRegistrationForm:countries']/option[@value='94']").click();
		driver.findElementByXPath("//input[@name='userRegistrationForm:email']").sendKeys("sathishcse99@gmail.com");
		Thread.sleep(2000);
		driver.findElementByXPath("//input[@name='userRegistrationForm:mobile']").sendKeys("7708485601");
		driver.findElementByXPath("//select[@name='userRegistrationForm:nationalityId']/option[@value='94']").click();
		driver.findElementByXPath("//input[@name='userRegistrationForm:address']").sendKeys("3/1");
		driver.findElementByXPath("//input[@name='userRegistrationForm:street']").sendKeys("Big sreet");
		driver.findElementByXPath("//input[@name='userRegistrationForm:area']").sendKeys("Cheyyar");
		driver.findElementByXPath("//input[@name='userRegistrationForm:pincode']").sendKeys("604407");
		Thread.sleep(6000);
		driver.findElementByXPath("//input[@name='userRegistrationForm:statesName']").sendKeys("Tamilnadu");
		Thread.sleep(5000);

		driver.findElementByXPath("//select[@name='userRegistrationForm:cityName']/option[@value='Tiruvannamalai']")
				.click();
		Thread.sleep(5000);

		driver.findElementByXPath("//select[@name='userRegistrationForm:postofficeName']/option[@value='Cheyyar B.O']")
				.click();

		driver.findElementByXPath("//input[@name='userRegistrationForm:landline']").sendKeys("044-220234");
	}

}
