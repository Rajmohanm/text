package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Testcase2  extends ProjectMethods {

	@Test
	public void editLead() {
		// TODO Auto-generated method stub
		Login();
	
		click(locateElement("linkText","Create Lead"));
		WebElement cpyname =  locateElement("id","createLeadForm_companyName");
		type(cpyname, "HCL");

		WebElement fname =  locateElement("id","createLeadForm_firstName");
		type(fname, "sathish");

		WebElement lname =  locateElement("id","createLeadForm_lastName");
		type(lname, "mani");
		WebElement CreateLead = locateElement("class","smallSubmit");
		click(CreateLead);
		
}
}
