package week3.day1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctcreg {

	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("sathish");
		driver.findElementById("userRegistrationForm:password").sendKeys("kumar");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("kumar");
		
		WebElement source = driver.findElementById("userRegistrationForm:securityQ");
		Select dd = new Select(source);
		dd.selectByVisibleText("What make was your first car or bike?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Honda CB Unicorn");
		
		WebElement source1 = driver.findElementById("userRegistrationForm:prelan");
		Select dd1 = new Select(source1);
		dd1.selectByValue("hi");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("sathish");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("kumar");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Mani");
		
		WebElement radio1 = driver.findElement(By.id("userRegistrationForm:gender:0"));
		radio1.click();
		WebElement ms = driver.findElement(By.id("userRegistrationForm:maritalStatus:1"));
		ms.click();
		
		WebElement day = driver.findElementByName("userRegistrationForm:dobDay");
		Select d1 = new Select(day);
		d1.selectByValue("27");
		
		WebElement month = driver.findElementByName("userRegistrationForm:dobMonth");
		Select m1 = new Select(month);
		m1.selectByValue("07");
		
		WebElement year = driver.findElementByName("userRegistrationForm:dateOfBirth");
		Select y1 = new Select(year);
		y1.selectByValue("1988");
		
		WebElement Occup = driver.findElementById("userRegistrationForm:occupation");
		Select Occ = new Select(Occup);
		Occ.selectByVisibleText("Private");
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("12345hps");
		driver.findElementById("userRegistrationForm:idno").sendKeys("12345hps");
		
		WebElement Count = driver.findElementById("userRegistrationForm:countries");
		Select sel = new Select(Count);
		sel.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("Sathish@gmail.com");

		driver.findElementById("userRegistrationForm:mobile").sendKeys("7708485601");
		
		WebElement Nation = driver.findElementById("userRegistrationForm:countries");
		Select n = new Select(Nation);
		n.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("1");
		driver.findElementById("userRegistrationForm:street").sendKeys("Bigstreet");
		driver.findElementById("userRegistrationForm:area").sendKeys("Chennai");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("604407", Keys.TAB);

		Thread.sleep(5000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select c = new Select(city);
		c.selectByValue("Tiruvannamalai");
		
		Thread.sleep(5000);
		
		WebElement poffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select po = new Select(poffice);
		po.selectByValue("Cheyyar B.O");
		Thread.sleep(5000);
		driver.findElementById("userRegistrationForm:landline").sendKeys("7708485601");
		
		WebElement radio2 = driver.findElement(By.name("userRegistrationForm:resAndOff"));
		radio2.click();
		
		
		
		
	}

}
