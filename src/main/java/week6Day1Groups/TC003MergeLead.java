package week6Day1Groups;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TC003MergeLead extends ProjectMethods {

	//@Test(groups = { "Regression" }, dependsOnGroups= {"sanity"})
	
	@Test(groups = { "Regression" })
	public void mergeLead() throws InterruptedException {
		click(locateElement("linkText", "Create Lead"));
		
		click(locateElement("linkText", "Merge Leads"));
		/*WebElement cpyname = locateElement("id", "createLeadForm_companyName");
		type(cpyname, "HCL");

		WebElement fname = locateElement("id", "createLeadForm_firstName");
		type(fname, "sathish");

		WebElement lname = locateElement("id", "createLeadForm_lastName");
		type(lname, "mani");
		WebElement CreateLead = locateElement("class", "smallSubmit");
		click(CreateLead);
		*/
		
		//driver.findElementByLinkText("Merge Leads").click();
		WebElement Merge = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(Merge);
		
	
		
		Set<String> window1 = driver.getWindowHandles();
		List<String> list1 = new ArrayList<>();
		list1.addAll(window1);
		driver.switchTo().window(list1.get(1));
		
		
		System.out.println(driver.getTitle());
		
		WebElement fname = locateElement("xpath", "//input[@name='firstName']");
		type(fname, "sathish");
		
		WebElement button = locateElement("xpath", "//button[text()='Find Leads']");
		click(button);
	
		Thread.sleep(2000);
		
	     driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	     driver.switchTo().window(list1.get(0));
	     
	     driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
	     
	     Set<String> window2 = driver.getWindowHandles();
	     List<String> list2 = new ArrayList<>();
	     list2.addAll(window2);
	     driver.switchTo().window(list2.get(1));
	     
	     
	     System.out.println(driver.getTitle());
	     Thread.sleep(2000);
	     driver.findElementByName("firstName").sendKeys("Kannan");
	     driver.findElementByXPath("//button[text()='Find Leads']").click();
	     Thread.sleep(2000);
	     
	     driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	     driver.switchTo().window(list2.get(0));
	     
	     
	     driver.findElementByClassName("buttonDangerous").click();
	     
	     Alert alert = driver.switchTo().alert();
	     String text = alert.getText();
	     System.out.println(text.contains("Are you sure?"));
	     if(text.contains("Are you sure?"))
	     {
	    	 alert.accept();
	     }
	     driver.findElementByXPath("//a[text()='Find Leads']").click();
		
	}
}
