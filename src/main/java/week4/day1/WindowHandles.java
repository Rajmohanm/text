package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandles {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
	//	driver.manage().timeouts().implicitlyWait(30, unit);
		
		driver.findElementByXPath("//a[text()='Contact Us']").click();
		Set<String> allwindow1 = driver.getWindowHandles();
		List<String> lst1 = new ArrayList<>();
		lst1.addAll(allwindow1);
		driver.switchTo().window(lst1.get(1));
		System.out.println(driver.getTitle());
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File obj = new File("./snap/img1.jpeg");
		FileUtils.copyFile(src, obj);
		driver.switchTo().window(lst1.get(0)).close();

	}

}
