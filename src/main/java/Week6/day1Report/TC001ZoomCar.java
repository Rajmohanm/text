package Week6.day1Report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Project.ProjectMethods;

public class TC001ZoomCar extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_Zoomcar";
		testDesc = "Find Max price of car ";
		author = "Sathish";
		category = "Sanity";
	}

	@Test
	public void findCar() {

		List<WebElement> prices = driver.findElementsByClassName("price");
		
		List<Integer> priceList = new ArrayList<>();
		for (int i = 0; i <= prices.size() - 1; i++) {

			String string = prices.get(i).getText();
			String str = string.replaceAll("\\D", "");
			int replaceAll = Integer.parseInt(str);
			priceList.add(replaceAll);
		}

		Set<Integer> set = new HashSet<>();
		set.addAll(priceList);
		int max = 0;
		for (int a : set) {
			if (a > max)
				max = a;

		}
		System.out.println("First maximum price " + max);

		String s = Integer.toString(max);
		
		String text = driver.findElementByXPath("//div[contains(text(),'920')]/preceding::div[12]/h3").getText();

		System.out.println(text);
		WebElement selectcar = locateElement("xpath", "//div[contains(text(),'920')]/following-sibling::button[text()='BOOK NOW']");
		click(selectcar);
	}
		
		
	}

