package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class AssigWindowHandles {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();

		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@type='text']").sendKeys("DemoSalesManager");

		driver.findElementByXPath("//input[@type='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@value='Login']").click();
		driver.findElementByXPath("//div[@id='button']//following::a[1]").click();
		driver.findElementByLinkText("Leads").click();
		// driver.findElementByXPath("//div[@class='frameSectionBody']//following::li[1]").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		
		Set<String> window1 = driver.getWindowHandles();
		List<String> list1 = new ArrayList<>();
		list1.addAll(window1);
		driver.switchTo().window(list1.get(1));
		
		
		System.out.println(driver.getTitle());
		
		
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Sathish");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		
	     driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	     driver.switchTo().window(list1.get(0));
	     
	     driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
	     
	     Set<String> window2 = driver.getWindowHandles();
	     List<String> list2 = new ArrayList<>();
	     list2.addAll(window2);
	     driver.switchTo().window(list2.get(1));
	     
	     
	     System.out.println(driver.getTitle());
	     Thread.sleep(2000);
	     driver.findElementByName("firstName").sendKeys("Kannan");
	     driver.findElementByXPath("//button[text()='Find Leads']").click();
	     Thread.sleep(2000);
	     
	     driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	     driver.switchTo().window(list2.get(0));
	     
	     
	     driver.findElementByClassName("buttonDangerous").click();
	     
	     Alert alert = driver.switchTo().alert();
	     String text = alert.getText();
	     System.out.println(text.contains("Are you sure?"));
	     if(text.contains("Are you sure?"))
	     {
	    	 alert.accept();
	     }
	     driver.findElementByXPath("//a[text()='Find Leads']").click();
	     driver.findElementByXPath("//input[@name='id']").sendKeys("abc");
	     driver.findElementByXPath("//button[text()='Find Leads']").click();
	     
	     driver.close();
	     
	  
	}

}
