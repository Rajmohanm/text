import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alerthandle {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/test/delete_customer.php");
		driver.findElementByName("cusid").sendKeys("1234");
		driver.findElementByName("submit").click();
		Alert alert = driver.switchTo().alert();
		
		String text = driver.switchTo().alert().getText();
		System.out.println(text);
		
		Thread.sleep(5000);
		alert.accept();
	
		
		
		

	}

}
