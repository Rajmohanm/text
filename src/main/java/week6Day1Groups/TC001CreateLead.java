package week6Day1Groups;

import org.junit.AfterClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import TestNG.ProjectMethods;

public class TC001CreateLead extends ProjectMethods {

	@Test(groups = { "smoke" })
	public void createLead() {

		click(locateElement("linkText", "Create Lead"));
		WebElement cpyname = locateElement("id", "createLeadForm_companyName");
		type(cpyname, "HCL");

		WebElement fname = locateElement("id", "createLeadForm_firstName");
		type(fname, "sathish");

		WebElement lname = locateElement("id", "createLeadForm_lastName");
		type(lname, "mani");
		WebElement CreateLead = locateElement("class", "smallSubmit");
		click(CreateLead);

	}

}
