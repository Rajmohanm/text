package ExtentReports;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger = extent.createTest("TC002_MergeLead", "Create a New Lead");
		logger.assignAuthor("Sathish");
		logger.assignCategory("smoke");
		logger.log(Status.PASS, "The Data DemoSalesManager Entered Successfully ", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
    
		logger.log(Status.PASS, "The Data crmsfa Entered Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
		logger.log(Status.PASS, "The login Button Clicked Successully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
	  extent.flush();
	}

}
